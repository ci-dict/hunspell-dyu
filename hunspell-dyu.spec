%if 0%{?fedora} >= 36 || 0%{?rhel} > 9
%global dict_dirname hunspell
%else
%global dict_dirname myspell
%endif

Name: hunspell-dyu
Summary: Dyula hunspell dictionaries
%global tag 0.5
Version: %{tag}
Release: 1%{?dist}
Source0: https://gitlab.com/ci-dict/hunspell-dyu/-/archive/%{tag}/hunspell-dyu-%{tag}.tar.gz
URL: https://coastsystems.net/blog/fr/blog/2022-07-28-hunspell-dyu/ 
License: GPLv3+
BuildArch: noarch
BuildRequires: unzip
Requires: hunspell-filesystem
Requires(post): %{_sbindir}/alternatives
Requires(postun): %{_sbindir}/alternatives
Supplements: (hunspell)

%description
Dyula hunspell dictionaries.

%prep
%autosetup

%build
for i in README.adoc LICENSE; do
  if ! iconv -f utf-8 -t utf-8 -o /dev/null $i > /dev/null 2>&1; then
    iconv -f ISO-8859-1 -t UTF-8 $i > $i.new
    touch -r $i $i.new
    mv -f $i.new $i
  fi
  tr -d '\r' < $i > $i.new
  touch -r $i $i.new
  mv -f $i.new $i
done

%install
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/%{dict_dirname}
cp -p abc/dyu_BF.dic $RPM_BUILD_ROOT/%{_datadir}/%{dict_dirname}/dyu-abc.dic
cp -p abc/dyu_BF.aff $RPM_BUILD_ROOT/%{_datadir}/%{dict_dirname}/dyu-abc.aff
cp -p ci/dyu_CI.dic $RPM_BUILD_ROOT/%{_datadir}/%{dict_dirname}/dyu-ci.dic
cp -p ci/dyu_CI.aff $RPM_BUILD_ROOT/%{_datadir}/%{dict_dirname}/dyu-ci.aff

%post
lang="dyu"
dyu_aliases="dyu_BF dyu_CI"
variantes="abc ci"
for alias in $dyu_aliases; do
	priority=0
	for variant in $variantes ; do
		priority=$[priority +10]
		%{_sbindir}/alternatives --install %{_datadir}/%{dict_dirname}/$alias.dic hunspell-$alias %{_datadir}/%{dict_dirname}/$lang-$variant.dic $priority \
		--slave %{_datadir}/%{dict_dirname}/$alias.aff $alias.aff %{_datadir}/%{dict_dirname}/$lang-$variant.aff
	done
	#%{_sbindir}/alternatives --auto hunspell-$alias
	#%{_sbindir}/alternatives --display hunspell-$alias
done

#set this manually so it doesnt initially point to ci
%{_sbindir}/alternatives --set hunspell-dyu_BF %{_datadir}/%{dict_dirname}/dyu-abc.dic
#%{_sbindir}/alternatives --list

%postun
if [ $1 -eq 0 ] ; then
dyu_aliases="dyu_BF dyu_CI"
for alias in $dyu_aliases; do
	  %{_sbindir}/alternatives --remove-all $alias
done
fi

%files
%doc README.adoc
%license LICENSE
%{_datadir}/%{dict_dirname}/*
%ghost %{_datadir}/%{dict_dirname}/dyu_BF.aff
%ghost %{_datadir}/%{dict_dirname}/dyu_BF.dic
%ghost %{_datadir}/%{dict_dirname}/dyu_CI.aff
%ghost %{_datadir}/%{dict_dirname}/dyu_CI.dic

%changelog
* Tue Sep 7 2022 Boyd Kelly bkelly@fedoraproject.org - 0.6
- Add basic aff file with plural 'w', and TRY for ci.

* Tue Jul 19 2022 Boyd Kelly bkelly@fedoraproject.org - 0.5
- Initial commit Add ci and abc variants and alternatives.

* Wed Jul 13 2022 Boyd Kelly <bkelly@fedoraproject.org> - .5
- hunspell for Dyula - initial build .5


files from abcburkina.net
unzipping the dico file will overwrite the current dyu_bf utf-8 files with <iso8859-1> files.
However these files are not recognized by iconv as iso8859-1.
The following replacement/conversions have been run to convert these to utf-8.
Ä > ɛ
Ì > ɔ
Í > ɲ
Ã > ŋ
